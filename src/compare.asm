;   Compare - Compare TIMER1 with CCPPR1 and send output
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Compare
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;Compare (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the compare counter
; - RB1 - 7 - bit 1 of the compare counter
; - RB2 - 8 - bit 2 of the compare counter
; - RA3 - 2 - bit 3 of the compare conter
; - RB4 - 10 - bit 4 of the compare counter
; - RB5 - 11 - bit 5 of the compare counter
; - RB6 - 12 - bit 6 of the compare counter
; - RB7 - 13 - bit 7 of the compare counter
; - RB3 - 9 - LED to indicate a match between TMR1 and CCPR1
;=============================

;Variables
;==============================
COUNTER EQU 0x27
TEMP EQU 0x28

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start		
	org 0x04 ;Interrupt vector
	goto Interrupt

;Functions
;==============================
	include digitecnology.inc

;Program
;==============================
Interrupt ;Interrupt routine
	bcf INTCON,7 ;Disable interrupts (for prevent an interrupt ocurring in an interrupt)
	call chgbnk0
	btfss PIR1,0 ;TIMER1 interrupt
	goto $+0x0F ;15
	incf COUNTER,1
	movf PORTB,0
	andlw b'00001000'
	movwf TEMP
	movf COUNTER,0
	andlw b'11110111'
	iorwf TEMP,0
	movwf PORTB
	btfss COUNTER,3
	goto $+3
	bsf PORTA,3
	goto $+2
	bcf PORTA,3
	bcf PIR1,0 ;Clear interrupt
	btfss PIR1,2 ;CCP1 interrupt
	goto $+7
	btfss PORTB,3
	goto $+3
	bcf PORTB,3
	goto $+2
	bsf PORTB,3
	bcf PIR1,2 ;Clear interrupt	
	bsf INTCON,7 ;Enable interrupts
	retfie
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	clrf TRISB ;Outputs
	bcf TRISA,3 ;Output
	;Deactivate analog comparators
	call chgbnk0
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0	
	;Configure TIMER1
 	bsf T1CON,5 ;Set 1:8 prescale value
	bsf T1CON,4
	bcf T1CON,3 ;Oscillator disables
	bcf T1CON,1 ;Use internal oscillator
	bsf T1CON,0 ;Enables TIMER1
	;Configure compare mode
	bsf CCP1CON,3 ;Compare mode: generate interrupt on match
	bcf CCP1CON,2
	bsf CCP1CON,1
	bcf CCP1CON,0
	;Configure CCPR1 register (put a 256 value)
	clrf CCPR1H
	movlw 0x80
	movwf CCPR1L
	;Clear counter and TIMER1
	clrf TMR1L
	clrf TMR1H
	clrf COUNTER
	;Configure interrupts
	call chgbnk1
	bsf PIE1,0 ;Enable TIMER1 interrupt
	bsf PIE1,2 ;Enable CCP1 interrupt
	call chgbnk0		
	bsf INTCON,6 ;Enable peripheral interrupts
	bsf INTCON,7 ;Enable interrupts	
Cycle			
	goto Cycle
	end
